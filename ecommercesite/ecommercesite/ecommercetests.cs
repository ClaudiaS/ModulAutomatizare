﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ecommercesite
{

[TestFixture] 
    public class ecommercetests
    {
        const String path = "D:\\Curs c#\\Automatizare\\Drivers";
        IWebDriver driver;

        [Test]

        public void LoginChrome()
        {
            driver = new ChromeDriver(@path);
            driver.Navigate().GoToUrl("https://www.carrefour.ro");
            driver.Manage().Window.Maximize();
            Thread.Sleep(3000);
            String Title = driver.Title;
            Console.WriteLine(" Chrome opened the page " + Title);

            driver.FindElement(By.CssSelector("div.clickable > h2")).Click();

            driver.FindElement(By.Id("email_header")).SendKeys("clstfn000@gmail.com");
            Thread.Sleep(2000);
            driver.FindElement(By.Id("parola_header")).SendKeys("Bani2018");
            Thread.Sleep(2000);


            driver.FindElement(By.Id("btnsubmit_header")).Click();
            Thread.Sleep(2000);
            String LoginText = driver.FindElement(By.CssSelector("#myCarrefour > p")).Text;
            Assert.IsTrue(LoginText.Contains("Bine ai venit in contul tau! "));
            Console.WriteLine(LoginText);
            driver.Close();  
        }

        [Test]
        public void ChromeTest()
        {
            
            driver = new ChromeDriver(@path);
            driver.Navigate().GoToUrl("https://www.carrefour.ro");
            driver.Manage().Window.Maximize();
            Thread.Sleep(3000);
            String Title = driver.Title;
            Console.WriteLine(" Chrome opened the page " + Title);

           

            driver.FindElement(By.CssSelector("div.clickable > h2")).Click();

           //am pus un waiting time pe care nu il voi folosi in deloc :)
            IWebElement wait1 = new WebDriverWait(driver, TimeSpan.FromSeconds(6)).Until(x => x.FindElement(By.Id("autentificare_header")));
   

            //Deschidere Creare cont
            driver.FindElement(By.CssSelector("[href*='mycarrefour/cont-nou/']")).Click();
            IWebElement wait2 = new WebDriverWait(driver, TimeSpan.FromSeconds(6)).Until(x => x.FindElement(By.ClassName("loginBox")));

            driver.FindElement(By.Id("nume")).SendKeys("Ion");

            driver.FindElement(By.Id("prenume")).SendKeys("Creanga");

            // Random unique email generator
 
             Random emailGen = new Random();
            int randomInt = emailGen.Next(1000);
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            new string(Enumerable.Repeat(chars, 10)
                  .Select(s => s[emailGen.Next(s.Length)]).ToArray());

            driver.FindElement(By.Id("email")).SendKeys( emailGen + "@gmail.com");

            driver.FindElement(By.Id("parola")).SendKeys("Bani2018");

            driver.FindElement(By.Id("confirmare_parola")).SendKeys("Bani2018");

            

            //methoda de scroll pentru a vedea elemente

            driver.FindElement(By.CssSelector("#termeni_da")).Click();

            driver.FindElement(By.Id("btnsubmit")).Click();

            String RegText = driver.FindElement(By.CssSelector("#div.loginBox")).Text;
            Assert.IsTrue(RegText.Contains("Contul tau a fost creat cu succes!"));
            Console.WriteLine(RegText);

            driver.Close();


        }

        [Test]

        public void LoginMozilla()
        {
            driver = new FirefoxDriver(@path);
            driver.Navigate().GoToUrl("https://www.carrefour.ro");
            driver.Manage().Window.Maximize();
            Thread.Sleep(3000);
            String Title = driver.Title;
            Console.WriteLine(" Chrome opened the page " + Title);

            driver.FindElement(By.CssSelector("div.clickable > h2")).Click();

            driver.FindElement(By.Id("email_header")).SendKeys("clstfn000@gmail.com");
            Thread.Sleep(2000);
            driver.FindElement(By.Id("parola_header")).SendKeys("Bani2018");
            Thread.Sleep(2000);


            driver.FindElement(By.Id("btnsubmit_header")).Click();
            Thread.Sleep(2000);
            String LoginText = driver.FindElement(By.CssSelector("#myCarrefour > p")).Text;
            Assert.IsTrue(LoginText.Contains("Bine ai venit in contul tau! "));
            Console.WriteLine(LoginText);
            driver.Close();
        }

        [Test]
        public void FireFoxTest()
        {

            driver = new FirefoxDriver(@path);
            driver.Navigate().GoToUrl("https://www.carrefour.ro");
            driver.Manage().Window.Maximize();
            Thread.Sleep(3000);
            String Title = driver.Title;
            Console.WriteLine(" Chrome opened the page " + Title);



            driver.FindElement(By.CssSelector("div.clickable > h2")).Click();

            //am pus un waiting time pe care nu il voi folosi in deloc :)
            IWebElement wait1 = new WebDriverWait(driver, TimeSpan.FromSeconds(6)).Until(x => x.FindElement(By.Id("autentificare_header")));
  

            //Deschidere Creare cont
            driver.FindElement(By.CssSelector("[href*='mycarrefour/cont-nou/']")).Click();
            IWebElement wait2 = new WebDriverWait(driver, TimeSpan.FromSeconds(6)).Until(x => x.FindElement(By.ClassName("loginBox")));

            driver.FindElement(By.Id("nume")).SendKeys("Ion");

            driver.FindElement(By.Id("prenume")).SendKeys("Creanga");

            // Random unique email generator

            Random emailGen = new Random();
            int randomInt = emailGen.Next(1000);
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            new string(Enumerable.Repeat(chars, 10)
                  .Select(s => s[emailGen.Next(s.Length)]).ToArray());

            driver.FindElement(By.Id("email")).SendKeys(emailGen + "@gmail.com");

            driver.FindElement(By.Id("parola")).SendKeys("Bani2018");

            driver.FindElement(By.Id("confirmare_parola")).SendKeys("Bani2018");

            driver.FindElement(By.CssSelector("#termeni_da")).Click();

            driver.FindElement(By.Id("btnsubmit")).Click();

            String RegText = driver.FindElement(By.CssSelector("#div.loginBox")).Text;
            Assert.IsTrue(RegText.Contains("Contul tau a fost creat cu succes!"));
            Console.WriteLine(RegText);

            driver.Close();
        }
    }
}
